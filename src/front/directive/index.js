/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
export default (Vue) => {
  Vue.directive('auth', {
    // 当绑定元素插入到 DOM 中。
    inserted: function(el, binding) {
      if (window.app.$store.getters.keys.indexOf(binding.value) === -1) {
        el.remove();
      }
    }
  });
  Vue.directive('auth-else', {
    // 当绑定元素插入到 DOM 中。
    inserted: function(el, binding) {
      if (window.app.$store.getters.keys.indexOf(binding.value) !== -1) {
        el.remove();
      }
    }
  });
};
