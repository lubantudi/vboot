/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */

export default {
  list: 'system/user/list',
  updateEnable: 'post:system/user/updateEnable',
  save: 'post:system/user/save',
  update: 'post:system/user/update',
  delete: 'system/user/delete',
  roleList: 'system/user/roleList',
  get: 'system/user/get',
  resetPassword: 'system/user/resetPassword'
};
