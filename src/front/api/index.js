/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
import system from './system';
import common from './common';
import Xhr from './xhr';

const api = {
  system,
  common
};

export default (Vue) => {
  Object.assign(Vue.prototype, {
    $api(method = '') {
      const $this = this;
      let mt = api;
      method.split('.').forEach((value) => {
        mt = mt[value];
      });

      if (!mt) {
        console.error(`未定义[${method}]接口`);
        throw new Error(`未定义[${method}]接口`);
      }

      return {
        invoke: function() {
          const xhr = new Xhr($this);
          if (typeof mt === 'string') {
            const ops = mt.split(':');
            const method = ops.length === 1 ? 'get' : ops[0];
            const url = ops.length === 1 ? ops[0] : ops[1];
            return xhr[method].apply(xhr, [url].concat(Array.from(arguments)));
          }
          return mt.apply($this, [xhr].concat(Array.from(arguments)));
        }
      };
    }
  });
};
