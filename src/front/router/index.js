import Vue from 'vue';
import VueRouter from 'vue-router';
import Layout from '../views/Layout';
import Login from '../views/Login';

import system from './system';
import common from './common';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Layout',
      meta: {
        navText: 'VBoot'
      },
      redirect: 'dashboard',
      component: Layout,
      children: [
        ...system,
        ...common
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login') {
    setTimeout(() => {
      window.app.$store.dispatch('init', window.app).then(() => {
        next();
      });
    }, 100);
  } else {
    next();
  }
});

export default router;
