package cn.gson.vboot.model.mapper;

import cn.gson.vboot.model.pojo.AcLog;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : cn.gson.vboot.model.mapper</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年09月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@Mapper
public interface AcLogMapper {

    /**
     * 获取日志列表
     *
     * @return
     */
    Page<AcLog> getLogs();

    /**
     * 插入日志
     *
     * @param acLog
     * @return
     */
    int insert(AcLog acLog);
}
